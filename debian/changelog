ruby-guard (2.18.1-3) unstable; urgency=medium

  * Team upload.
  * d/ruby-tests.rake: exclude some test not working with ruby 3.2

 -- Lucas Kanashiro <kanashiro@debian.org>  Tue, 27 Feb 2024 18:16:19 -0300

ruby-guard (2.18.1-2) unstable; urgency=medium

  * d/p/ignore-unicode-emoji-ambiguous-whitespaces-with-formatador-0.3+.patch:
    New patch. Ignore unicode emoji ambiguous whitespaces with formatador 0.3+.
    See also:
      - https://github.com/guard/guard/pull/986#issuecomment-1303991720
      - https://github.com/geemus/formatador/pull/26
      - https://github.com/hamano/locale-eaw (in Japanese)

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Tue, 26 Sep 2023 14:41:28 +0900

ruby-guard (2.18.1-1) unstable; urgency=medium

  * New upstream version 2.18.1
  * Update Standards-Version to 4.6.2.

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Fri, 22 Sep 2023 13:06:38 +0900

ruby-guard (2.18.0-3) unstable; urgency=medium

  * fix FTBFS with rspec-mock 3.12 and ruby 3+ (Closes: #1027075).
  * update Homepage/Source due to domain expiration.
  * Drop XS-Ruby-Version and XB-Ruby-Version.

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Wed, 28 Dec 2022 12:11:46 +0900

ruby-guard (2.18.0-2) unstable; urgency=medium

  [ Daniel Leidert ]
  * Fix watch file

  [ HIGUCHI Daisuke (VDR dai) ]
  * Update Standards-Version to 4.6.1.
  * eliminate lintian warning: update-debian-copyright

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Sun, 03 Jul 2022 11:02:18 +0900

ruby-guard (2.18.0-1) unstable; urgency=medium

  [ Cédric Boutillier ]
  * [ci skip] Update team name

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Jenkins ]
  * Remove constraints unnecessary since buster

  [ HIGUCHI Daisuke (VDR dai) ]
  * New upstream version 2.17.0
  * New upstream version 2.18.0
  * d/p/bump-pry-version.patch: removed, applied upstream.
  * eliminate lintian error: ruby-script-but-no-ruby-dep
  * eliminate lintian pedantic: duplicate-in-relation-field
  * Bump debhelper from old 12 to 13.

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Sat, 09 Oct 2021 17:50:08 +0900

ruby-guard (2.16.2-1) unstable; urgency=medium

  * Team upload
  * New upstream version 2.16.2
  * Add patch to fix regression wrt pry (Closes: #954724)
  * Refresh d/patches
  * Fix package wrt cme

 -- Utkarsh Gupta <utkarsh@debian.org>  Thu, 02 Apr 2020 14:45:50 +0530

ruby-guard (2.16.1-2) unstable; urgency=medium

  * Team upload
  * Refresh packaging files with a new run of dh-make-ruby
    - adds the proper dependency on ruby-rspec (Closes: #952090)
  * autopkgtest: drop hardcoded call to gem2deb-test-runner
  * debian/rules: drop installation of bin/_guard-core completely, instead of
    moving it outside of /usr/bin. It does not do anything useful.

 -- Antonio Terceiro <terceiro@debian.org>  Sun, 23 Feb 2020 14:25:21 -0300

ruby-guard (2.16.1-1) unstable; urgency=medium

  * New upstream version 2.16.1
  * d/salsa-ci.yml: disable salsa-ci blhc and build package any tests.
  * d/control: set Rules-Requires-Root: as no.
  * refresh patch.
  * Update Standards-Version to 4.4.1.

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Wed, 06 Nov 2019 22:20:50 +0900

ruby-guard (2.15.1-1) unstable; urgency=medium

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ HIGUCHI Daisuke (VDR dai) ]
  * New upstream version 2.15.1
  * d/{control,copyright}: use https instead of http
  * d/control: Bump debhelper compatibility level to 12 and
    use debhelper-compat instead of d/compat.
  * bump up Standards-Version to 4.4.0
  * refresh patches.

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Thu, 05 Sep 2019 18:33:11 +0900

ruby-guard (2.15.0-3) unstable; urgency=high

  * add workarounds for cli inheriting from thor (Closes: #926826)
  * set urgency=high due to RC bug fix.

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Mon, 29 Apr 2019 21:04:59 +0900

ruby-guard (2.15.0-2) unstable; urgency=medium

  * depends pry (>= 0.12) (Closes:: #914003)
  - d/p/compat_pry_0.11_input_array.patch: removed.

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Sun, 18 Nov 2018 22:51:21 +0900

ruby-guard (2.15.0-1) unstable; urgency=medium

  * New upstream version 2.15.0
  * d/rules: move _guard-core from /usr/bin to /usr/lib/ruby/vendor_ruby/guard.
  * d/p/compat_pry_0.11_input_array.patch: new patch.
  * Refresh patches.
  * bump up Standards-Version 4.2.1

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Sat, 17 Nov 2018 16:48:53 +0900

ruby-guard (2.14.2-2) unstable; urgency=medium

  * d/p/do_not_use_gem_bin_path.patch: use full path for _guard-core.
  * eliminate lintian warning: insecure-copyright-format-url
  * Move Vcs-* to salsa.debian.org
  * Bump debhelper compatibility level to 11.

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Sat, 03 Mar 2018 12:04:08 +0900

ruby-guard (2.14.2-1) unstable; urgency=medium

  * New upstream release.
  * d/watch: watch github release instead of rubygem due to lacking specs.
  * d/p/add_specs.patch: remove due to import upstream source with specs.
  * d/p/do_not_use_git_ls-files.patch: do not use `git ls-files` in gemspec.
  * refresh patch.
  * bump up Standards-Version 4.1.3.

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Thu, 04 Jan 2018 18:35:33 +0900

ruby-guard (2.14.1-1) unstable; urgency=medium

  * Initial release (Closes: #881992)

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Tue, 28 Nov 2017 17:01:25 +0900
