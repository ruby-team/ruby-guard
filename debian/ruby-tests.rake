require 'gem2deb/rake/spectask'

exclude = [
  './spec/lib/guard/cli/environments/*_spec.rb',
  './spec/lib/guard/guardfile/*_spec.rb',
  './spec/lib/guard/commands/change_spec.rb',
  './spec/lib/guard/dsl_describer_spec.rb',
]

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = './spec/**/*_spec.rb'
  spec.exclude_pattern = exclude.join(',')
end
